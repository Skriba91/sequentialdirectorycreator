import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SDC implements SDCInterface {
	
	private Path parentDirectory;
	private boolean exists;
	private String directoryName;
	private int numberOfDirectories;
	private int numberOfDigits;
	//TODO  boolean postfix
	//L�trehozni egy v�ltoz�t, ami azt jelzi, hogy a sorsz�moz�s pre- vagy postfix legyen
	//�s a hozz� tartoz� f�ggb�nyeket
	

	@Override
	public void SetdDirectoryPath(String path) {
		parentDirectory = Paths.get(path);
		exists = Files.exists(parentDirectory);
	}

	@Override
	public void SetDirectoryName(String name) {
		directoryName = "\\" + name;
	}

	@Override
	public void SetNumberOfDirectories(int number) {
		numberOfDirectories = number;
		numberOfDigits = NumberOfDigits(number);
	}

	@Override
	public boolean isExists() {
		return exists;
	}
	
	@Override
	public boolean Generate() {
		String actualName = new String(); //Seg�dv�ltoz� az aktu�lus mappa el�r�si �tvonal�nak t�rol�s�hoz
		//TODO Befejezni ezt a szart
		try{
			if(!Files.exists(parentDirectory)) {
				Files.createDirectories(parentDirectory);
			}
			
			for(int i = 0; i < numberOfDirectories; ++i) {
				//El��ll�tja az aktu�lis mappa nev�t, ami �ll az el�r�si �tvonalb�l, 
				//a mappa nev�b�l, kieg�sz�t� null�kb�l �s a mappa sorsz�m�b�l
				actualName = parentDirectory.toString() + directoryName + PostFixZeros(i+1) + (i+1);
				Files.createDirectory(Paths.get(actualName));
			}
		}
		catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * This method is always called by the SetNumberOfDirectories method. It sets how many decimal
	 * digits has the numberOfDirecties parameter. It is used by the class to calculate how many
	 * zeros needed in the postfix in the directories
	 * @param number The given number from the user of how many directories have to create the program
	 * @return The decimal digits of the parameter number.
	 * @throws IllegalArgumentException
	 */
	private int NumberOfDigits(int number) throws IllegalArgumentException {
		//Hiba gener�l�dik, ha egyn�l kevesebb mapp�t k�ne l�trehozni
		if(number < 1)
			throw new IllegalArgumentException("The given number can not be lesser than one");
		int digits = 0; //Null�r�l indul, k�l�nben a for ciklusban null�val k�ne osztani
		//Ameddig a mapp�k sz�ma elosztva a fut� v�ltoz�val nagyobb-egyenl� mint egy,
		//addig n�veli a digitek sz�m�t
		for(int i = 1; (number/i) >= 1; i*=10, ++digits); //Egyszer mindenk�pp lefut
		return digits;
	}
	
	//TUDO Hib�s, ezt meg kell m�g csin�lni, hogy ne fix sz�m� 0 rakjon mindenhova
	private String PostFixZeros(int directoryNumber) {
		String returnString = new String(""); //Seg�dv�ltoz� a v�gleges el�r�si �tvonalhoz
		for(int i = NumberOfDigits(directoryNumber); i < numberOfDigits; ++i)
			returnString += "0";
		return returnString;
	}
	
	
}
