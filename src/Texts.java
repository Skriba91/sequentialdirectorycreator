
public class Texts {

	public static final String WELCOME = "Sequencial directory generator.";
	public static final String REQUESTDIRECTORYPATH = "Give the path where to create directories: ";
	public static final String REQUESTBASENAME = "Give the base name of directories: ";
	public static final String GETNUMBEROFDIRECTORIES = "Give a valid (>=1) number of how many directories do you want to create: ";
	public static final String INVALIDPATH = "Given path does not exists.\nDo you want the program to create"
			+ " the path with all parent directories? Y/n";
	public static final String INVALIDNUMBER = "Invalid number!";
	public static final String SUCCESS = "Directories were created successfully :)";
	public static final String FAILURE = "Something went wrong :(";
	
}
