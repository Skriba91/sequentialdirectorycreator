import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 */

/**
 * @author Skriba
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		//�dv�zl� �zenet
		System.out.println(Texts.WELCOME);
		try (BufferedReader input = 
				new BufferedReader(new InputStreamReader(System.in))) {
			//P�ld�nyos�tja a v�grehajt�shoz sz�ks�ges oszt�lyt
			SDCInterface autoGenerate = new SDC();
			//Lek�rdezi az el�r�si �tvonalat
			//TODO felk�sz�teni, a programot, hogy ellen�rizze az el�r�si �tvonal �rv�nyess�g�t
			System.out.print(Texts.REQUESTDIRECTORYPATH);
			autoGenerate.SetdDirectoryPath(input.readLine());
			//Ha m�g nem l�tezik az �tvonal figyelmezteti a felhaszn�l�t �s utas�t�st v�r
			if(!autoGenerate.isExists()) {
				String command; //Seg�dv�ltoz� a felhaszn�l�i parancs t�rol�s�ra
				do {
					//Figyelmez�tet�s �s utas�t�s k�r�se, majd beolvas�sa �s kisbet�ss� konvert�l�sa.
					System.out.println(Texts.INVALIDPATH);
					command = new String(input.readLine().toLowerCase());
					//Ha a felhaszn�l� �j el�r�si �tvonalat szeretne, akkor elk�ri azt
					if(command.equals("n")) {
						System.out.print(Texts.REQUESTDIRECTORYPATH);
						autoGenerate.SetdDirectoryPath(input.readLine());
					}
				}
				//Ameddig a felhaszn�l� nem engedi, hogy a program l�trehozza az el�r�si �tvonalat, 
				//�s ameddig �rv�nytelen az �tvonal �jra figyelmezteti a felhaszn�l�t
				while(!autoGenerate.isExists() && command.equals("n"));
			}
			//Bek�ri a mappa nev�t
			System.out.print(Texts.REQUESTBASENAME);
			autoGenerate.SetDirectoryName(input.readLine());
			//Bek�ri h�ny mapp�t szeretne l�trehozni a felhaszn�l�
			//TODO �rv�nyes mappa n�v ellen�rz�se
			int number; //seg�dv�ltoz� mappasz�m beolvas�s�hoz
			do {
				System.out.print(Texts.GETNUMBEROFDIRECTORIES);
				number = Integer.parseInt(input.readLine());
				//Figyelmezteti a felhaszn�l�t az �rv�nytelen sz�mr�l
				if(number < 1) {
					System.out.println("\n" + Texts.INVALIDNUMBER);
				}
			}
			//Addig k�ri be a sz�mot a felhaszn�l�t�l, ameddig nem ad meg �rv�nyeset.
			while(number < 1);
			autoGenerate.SetNumberOfDirectories(number);
			//Ha minden k�sz kiadja a elk�sz�t�shez a prancsot
			if(autoGenerate.Generate()) {
				System.out.println(Texts.SUCCESS);
			}
			else {
				System.out.println(Texts.FAILURE);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
			System.out.println(Texts.FAILURE);
		}
		
		//runGUI();
		
	}
	
	@SuppressWarnings("unused")
	private static void runGUI() {
		
		//EventQueue.invokeLater(new CreateDirectories());
	}
	
	

}
