

/**
 * 
 * @author Skriba
 *
 */
public interface SDCInterface {

	/**
	 * Sets the path where the directories will be created.
	 * If no such path exsists it will create the all parent directories.
	 * @param path The path where the program will automaticly generates the directories
	 */
	public void SetdDirectoryPath(String path);
	
	/**
	 * Sets the name for the auto generated directories
	 * @param name The designated name for auto generated directories
	 */
	public void SetDirectoryName(String name);
	
	/**
	 * Sets how many directories will be generated
	 * @param number The number of how many directories will be generated.
	 */
	public void SetNumberOfDirectories(int number);
	
	/**
	 * 
	 * @return
	 */
	public boolean isExists();
	
	/**
	 * Automaticaly generates the the given number of directories
	 * to the defined path
	 * @return Returns true if the auto generation was succsessful
	 */
	public boolean Generate();
	
}
